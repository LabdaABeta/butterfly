data class WorldMap(
    val controlledLand: Map<Region, Nation> // When map takes two arguments can also be seen as Map(Key,Value)
) {
    fun landPercentage(nation: Nation) =
        // for each entry of (Region[key], Who controls it[value])
        controlledLand
            // Take only those regions that are owned by "nation"
            .filterValues { it == nation }
            // Take just the Region[key] since they are all controlled by nation now
            .keys
            // And add up their areas
            .sumByDouble { it.area }
}