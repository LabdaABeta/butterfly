import com.soywiz.klock.blockingSleep
import com.soywiz.klock.seconds
import com.soywiz.korev.Key
import com.soywiz.korge.*
import com.soywiz.korge.input.KeysEvents
import com.soywiz.korge.input.keys
import com.soywiz.korge.input.mouse
import com.soywiz.korge.input.onClick
import com.soywiz.korge.ui.KorgeNativeUiFactory
import com.soywiz.korge.view.solidRect
import com.soywiz.korge.view.text
import com.soywiz.korim.color.Colors
import com.soywiz.korim.font.nativeSystemFontProvider
import com.soywiz.korma.geom.topLeft
import com.soywiz.korui.UiApplication
import kotlin.random.Random

suspend fun main() = Korge(width = 512, height = 512, bgcolor = Colors["#2b2b2b"]) {

    val baseColor = Colors["#2b2b2b"]
    val random = Random(0)

    val rectangle = solidRect(512, 512) { x = globalBounds.left; y = globalBounds.top }

    rectangle.mouse.onClick {
        if (mouse.button.isLeft) {
            val x = random.nextBoolean()
            if (x) {
                rectangle.color = Colors["#FF0000"]
                println("Red")
            } else {
                rectangle.color = baseColor
                println("Grey")
            }
        }
    }
    keys.down {
        if (it.key == Key.ESCAPE) {
            gameWindow.exit()
        }
    }


    val t = text("stuff"){

    }

}

