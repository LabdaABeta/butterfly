data class Power(
    val strength : Int,
    val resources : Map<Resource, Int>
)
