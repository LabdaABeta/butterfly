data class Nation(
    // Writing variables like below is the same as writing variables as private or protected
    // Because "Nation" is a data class getters and setters are given automatically
    val power : Power,
    val name : String

)
