enum class Resource {
    WATER,
    WOOD,
    STONE,
    LIVESTOCK
}
