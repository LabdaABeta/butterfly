data class WorldState(
    val nations: List<Nation>,
    val map : WorldMap
) {
    fun apply(change: (WorldState) -> WorldState) = change(this)
}

